<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/DomocenterTable.php';

class Actions_Plugin_Domocenter extends Actions_Plugin_Skeleton
{
    private $_commands; 

    public function __construct ()
    {
        $this->_name        = 'Domocenter';
        $this->_commands    = array (
                            array ('id'     => 'domo001',
                                   'name'   => $this->getTranslation ('001'),
                                   'cmd'    => 'domocenter.cmd -m 001'),

                            array ('id'     => 'domo002',
                                   'name'   => $this->getTranslation ('002'),
                                   'cmd'    => 'domocenter.cmd -m 002'),

                            array ('id'     => 'domo003',
                                   'name'   => $this->getTranslation ('003'),
                                   'cmd'    => 'domocenter.cmd -m 003')
                            );
    }

    public function getName ()
    {
        return ($this->getTranslation ('Domo Center'));
    }

    public function getCommands ()
    {
        return ($this->_commands);
    }

    public function getContextProperties ($context)
    {
        // Get properties
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'gr':
                $properties = $this->getGroupProperties ($id);
                break;
            case 'box':
                return (NULL);
            default:
                $properties = $this->getDefaultUnknownProperties ($context);
        }

        return ($properties->getHTMLValues ());
    }

    protected function getGroupProperties ($id)
    {
        $dcTable = new Actions_Plugin_DomocenterTable ();
        $result = $dcTable->get ("gr-$id");
        if ($result == false)
        {
            $dcTable->set ("gr-$id", "example.com", "80");
            $result = $dcTable->get ("gr-$id");
        }

        $htmlProperties = array ();
        $htmlProperties [] = $this->createTextProperty (
                                "target",
                                $this->getTranslation ("Target hostname"),
                                $result ['target'],
                                $this->getTranslation ("Target, ex: 'www.example.com''"));
        $htmlProperties [] = $this->createTextProperty (
                                "port",
                                $this->getTranslation ("Target port"),
                                $result ['port'],
                                $this->getTranslation ("Port to contact, ex: '443'"));

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ("gr-".$id);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    protected function getDefaultUnknownProperties ($context)
    {
        $htmlProperties = array ();
        $htmlProperties [] = $this->createLabel ("Unknown context: $context.", '#aa0000');

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ($context);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    public function setParamGroup ($id, $param, $value)
    {
        // Set new param for all boxes in the group
        $tableBoxes = new Players_Boxes ();
        $boxes = $tableBoxes->getBoxesArrayByGroup ($id);
        foreach ($boxes as $box)
            $this->setParamBox ($box ['box_hash'], $param, $value);

        // Set the param for the group, in order to display it
        return ($this->setParam ("gr-$id", $param, $value));
    }

    public function setParamBox ($hash, $param, $value)
    {
        return ($this->setParam ($hash, $param, $value));
    }

    protected function setParam ($id, $param, $value)
    {
        $dcTable = new Actions_Plugin_DomocenterTable ();
        switch ($param)
        {
            case "target":
                $dcTable->setTarget ($id, $value);
                break;
            case "port":
                $dcTable->setPort ($id, $value);
                break;
        }
    }

    public function getStringCommand ($id)
    {
        foreach ($this->_commands as $command)
        {
            if (strcmp ($command ['id'], $id) == 0)
                return ($command ['cmd']);
        }
        return ("");
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        $dcTable = new Actions_Plugin_DomocenterTable ();
        $result = $dcTable->get ("$hash");
        if (!$result)
        {
            // Synchronize box with its group
            $tableBoxes = new Players_Boxes ();
            $box = $tableBoxes->findiBoxWithHash ($hash);
            if (!$box)
                return (false);

            $groupResult = $dcTable->get("gr-".$box->getGroupId ());
            if (!$groupResult)
                return (false);

            $dcTable->set ($hash, $groupResult['target'], $groupResult['port']);
            $result = $dcTable->get ("$hash");
            if (!$result)
                return (false);
        }

        if ($result ['updated'] == 1)
        {
            if ($result ['target'] && $result ['port'])
            {
                $toSend = array (
                            'cmd'    => 'PLUGIN',
                            'params' => "domocenter.cmd -t ".$result ['target']." -p ".$result ['port']);
            }

            $dcTable->setRead ($hash);
            return ($toSend);
        }

        return (false);
    }
}
