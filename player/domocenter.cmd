#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====

#
# Variables
#
CONF=/var/lib/iplayer/cmd/domocenter.conf

###############################################################################
# Log everything as player does
function iInfo
{
    logger -p local4.info -t Domocenter -- "$@"
}
function iError
{
    logger -p local4.err -t Domocenter -- "$@"
}

function usage
{
    echo "Usage:"
    echo "$0 <cmd>"
    echo ""
    echo "With <cmd> in:"
    echo "  -h                      Display this usage screen"
    echo "  -m <seq>                Sequence to ask to the target"
    echo "  -t <target>             Set the URL to use"
}

function failure
{
    iError "$1"
    exit 1
}

function getMyConfFile ()
{
    echo "$CONF"
}

function parseConf ()
{
    CONFFILE=$( getMyConfFile )
    
    if [ ! -f "$CONFFILE" ];
    then
        TARGET="example.com"
        PORT="80"
        saveConf
    fi

    TARGET=`grep -E '^TARGET=' $CONFFILE | head -n1 | sed -r 's/^TARGET=//g'`
    PORT=`grep -E '^PORT=' $CONFFILE | head -n1 | sed -r 's/^PORT=//g'`
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "TARGET=${TARGET}
PORT=${PORT}
" > $CONFFILE

    if [ "$EUID" = "0" ];
    then
        chown player:player $CONFFILE
    fi
}

###############################################################################

# This script use player permissions, even if launched as root.
if [ "${EUID}" = "0" ];
then
    iInfo "Running $( basename $0 ) as user 'player'..."
    if [[ x = x$@ ]] ; then
        sudo -u player $0
    else
        sudo -u player $0 "$@"
    fi
    exit $?
fi

parseConf

# What should we do ?
flag=
bflag=
while getopts 'hm:t:p:' OPTION ; do
    case ${OPTION} in
        h)
            usage $( basename $0 )
            exit 0
            ;;
        m)
            # Send a query, don't care about the result
            SEQUENCE="${OPTARG}"
            echo "${SEQUENCE}" | nc -q 1 ${TARGET} ${PORT} &>/dev/null
            ;;
        t)
            # Configure a new target
            TARGET="${OPTARG}"
            saveConf
            ;;
        p)
            # Configure a new port
            PORT="${OPTARG}"
            saveConf
            ;;
        *)
            usage $( basename $0 )
            exit 2
            ;;
    esac
done

# vi:syntax=sh
